// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph {
    public:
        // ------
        // usings
        // ------

        using vertex_descriptor = int;          // fix!
        using edge_descriptor   = std::pair<vertex_descriptor,vertex_descriptor>;          // fix!

        using vertex_iterator    = std::vector<vertex_descriptor>::const_iterator;        // fix!
        using edge_iterator      = std::vector<edge_descriptor>::const_iterator;        // fix!
        using adjacency_iterator = std::vector<vertex_descriptor>::const_iterator;        // fix!

        using vertices_size_type = std::size_t;
        using edges_size_type    = std::size_t;

    public:
        // --------
        // add_edge
        // --------

        /**
         * your documentation
         */
        friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor a, vertex_descriptor b, Graph& g) {
            // <your code>
            edge_descriptor ed = std::make_pair(a,b);
            bool add = true;
            vertex_iterator ei = g.g[a].begin();
            while(ei != g.g[a].end()){
                if(*ei == b){
                    add = false;
                }
                ++ei;
            }                        
            if(add){                
                g.g[a].push_back(b);
                g.edge.push_back(ed);                
                return std::make_pair(ed,true);
            }
            return std::make_pair(ed, false);}

        // ----------
        // add_vertex
        // ----------

        /**
         * your documentation
         */
        friend vertex_descriptor add_vertex (Graph& g) {
            // <your code>
            vertex_descriptor v = g.g.size(); // fix
            g.g.push_back(std::vector<vertex_descriptor>());
            g.vertex.push_back(v);
            return v;}

        // -----------------
        // adjacent_vertices
        // -----------------

        /**
         * your documentation
         */
        friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor a, const Graph& g) {
            // <your code>            
            adjacency_iterator b = g.g[a].begin();
            adjacency_iterator e = g.g[a].end();
            return std::make_pair(b, e);}

        // ----
        // edge
        // ----

        /**
         * your documentation
         */
        friend std::pair<edge_descriptor, bool> edge (vertex_descriptor a, vertex_descriptor b, const Graph& g) {
            // <your code>
            edge_descriptor ed = std::make_pair(a,b);            
            edge_iterator ei = g.edge.begin();
            while(ei != g.edge.end()){
                if(*ei == ed){
                    return make_pair(ed, true);
                }
                ++ei;
            }
            return std::make_pair(ed, false);}

        // -----
        // edges
        // -----

        /**
         * your documentation
         */
        friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
            // <your code>
            edge_iterator b = g.edge.begin();
            edge_iterator e = g.edge.end();
            return std::make_pair(b, e);}

        // ---------
        // num_edges
        // ---------

        /**
         * your documentation
         */
        friend edges_size_type num_edges (const Graph& g) {
            // <your code>
            edges_size_type s = g.edge.size(); // fix
            return s;}

        // ------------
        // num_vertices
        // ------------

        /**
         * your documentation
         */
        friend vertices_size_type num_vertices (const Graph& g) {
            // <your code>
            vertices_size_type s = g.vertex.size(); // fix
            return s;}

        // ------
        // source
        // ------

        /**
         * your documentation
         */
        friend vertex_descriptor source (edge_descriptor a, const Graph& g) {
            // <your code>
            vertex_descriptor v = a.first; // fix
            return v;}

        // ------
        // target
        // ------

        /**
         * your documentation
         */
        friend vertex_descriptor target (edge_descriptor a, const Graph& g) {
            // <your code>
            vertex_descriptor v = a.second; // fix
            return v;}

        // ------
        // vertex
        // ------

        /**
         * your documentation
         */
        friend vertex_descriptor vertex (vertices_size_type a, const Graph& g) {
            // <your code>
            vertex_descriptor vd = g.vertex[a]; // fix
            return vd;}

        // --------
        // vertices
        // --------

        /**
         * your documentation
         */
        friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
            // <your code>
            vertex_iterator b = g.vertex.begin();
            vertex_iterator e = g.vertex.end();
            return std::make_pair(b, e);}

    private:
        // ----
        // data
        // ----

        std::vector<std::vector<vertex_descriptor>> g; // something like this
        std::vector<vertex_descriptor> vertex;
        std::vector<edge_descriptor> edge;

        // -----
        // valid
        // -----

        /**
         * your documentation
         */
        bool valid () const {
            if(vertex.size() < g.size()){
                return false;
            }
            return true;}

    public:
        // --------
        // defaults
        // --------

        // Graph             ()             = default;
        // Graph             (const Graph&) = default;
        // ~Graph            ()             = default;
        // Graph& operator = (const Graph&) = default;};

        Graph(std::vector<std::vector<vertex_descriptor>> _g = std::vector<std::vector<vertex_descriptor>>(), 
            std::vector<vertex_descriptor> _v = std::vector<vertex_descriptor>(),
            std::vector<edge_descriptor> _e = std::vector<edge_descriptor>()):g(_g),vertex(_v),edge(_e){}
    };

#endif // Graph_h
